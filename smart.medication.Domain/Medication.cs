﻿using System;
using System.ComponentModel.DataAnnotations;

namespace smart.medication.Domain
{
    public class Medication
    {
        [Key]
        public int MedicationId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public double Quantity { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }
    }
}
