﻿using Microsoft.EntityFrameworkCore;
using smart.medication.Domain;
using smart.medication.Persistence.Configurations.Entities;

namespace smart.medication.DbContexts
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Medication> Medications { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new MedicationConfiguration());
        }
    }
}
