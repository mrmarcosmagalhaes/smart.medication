﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using smart.medication.Domain;

namespace smart.medication.Persistence.Configurations.Entities
{
    public class MedicationConfiguration : IEntityTypeConfiguration<Medication>
    {
        public void Configure(EntityTypeBuilder<Medication> builder)
        {
            builder.HasData(new Medication
            {
                MedicationId = 1,
                Name = "Benuron",
                Quantity = 10,
                CreationDate = System.DateTime.Now
            },
            new Medication
            {
                MedicationId = 2,
                Name = "Brufen",
                Quantity = 20,
                CreationDate = System.DateTime.Now
            });
        }
    }
}
