﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace smart.medication.Persistence.Migrations
{
    public partial class AddMedicationDbModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Medications",
                columns: table => new
                {
                    MedicationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Quantity = table.Column<double>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medications", x => x.MedicationId);
                });

            migrationBuilder.InsertData(
                table: "Medications",
                columns: new[] { "MedicationId", "CreationDate", "Name", "Quantity" },
                values: new object[] { 1, new DateTime(2022, 6, 23, 13, 26, 23, 904, DateTimeKind.Local).AddTicks(6010), "Benuron", 10.0 });

            migrationBuilder.InsertData(
                table: "Medications",
                columns: new[] { "MedicationId", "CreationDate", "Name", "Quantity" },
                values: new object[] { 2, new DateTime(2022, 6, 23, 13, 26, 23, 909, DateTimeKind.Local).AddTicks(4470), "Brufen", 20.0 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Medications");
        }
    }
}
