﻿using smart.medication.Application.Contracts.Persistence;
using smart.medication.DbContexts;
using System;
using System.Threading.Tasks;

namespace smart.medication.Persistence.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        
        private IMedicationRepository _medicationRepository;

        public UnitOfWork(ApplicationDbContext context, IMedicationRepository medicationRepository)
        {
            _context = context;
            _medicationRepository = medicationRepository;
        }

        public IMedicationRepository MedicationRepository => 
            _medicationRepository ??= new MedicationRepository(_context);

        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }
    }
}
