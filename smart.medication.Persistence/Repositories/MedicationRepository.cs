﻿using Microsoft.EntityFrameworkCore;
using smart.medication.Application.Contracts.Persistence;
using smart.medication.DbContexts;
using smart.medication.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace smart.medication.Persistence.Repositories
{
    public class MedicationRepository : IMedicationRepository
    {
        private readonly ApplicationDbContext _db;

        public MedicationRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<Medication>> GetAll()
        {
            return await _db.Medications.ToListAsync();
        }

        public async Task<Medication> Add(Medication medication)
        {
            medication.CreationDate = System.DateTime.Now;
            await _db.Medications.AddAsync(medication);
            return medication;
        }

        public void Delete(Medication medication)
        {
            _db.Set<Medication>().Remove(medication);
        }

        public async Task<Medication> Get(int medicationId)
        {
            return await _db.Set<Medication>().FindAsync(medicationId);
        }
    }
}
