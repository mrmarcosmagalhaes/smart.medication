﻿using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using smart.medication.Application.DTOs.Validators;
using smart.medication.Application.Mapping;
using smart.medication.Application.Models.Dto;
using System.Reflection;

namespace smart.medication.Application
{
    public static class ApplicationServicesRegistration
    {
        public static IServiceCollection ConfigureApplicationServices(this IServiceCollection services)
        {
            IMapper mapper = MappingConfig.RegisterMaps().CreateMapper();

            services.AddScoped<IValidator<MedicationDto>, CreateMedicationDtoValidator>();

            services.AddSingleton(mapper);
            services.AddMediatR(Assembly.GetExecutingAssembly());
            return services;
        }
    }
}
