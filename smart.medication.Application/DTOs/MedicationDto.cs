﻿using System;

namespace smart.medication.Application.Models.Dto
{
    public class MedicationDto
    {
        public int MedicationId { get; set; }
       
        public string Name { get; set; }
        
        public int Quantity { get; set; }
        
        public DateTime CreationDate { get; set; }
    }
}
