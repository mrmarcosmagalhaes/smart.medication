﻿using FluentValidation;
using smart.medication.Application.Models.Dto;

namespace smart.medication.Application.DTOs.Validators
{
    public class CreateMedicationDtoValidator : AbstractValidator<MedicationDto>
    {
        public CreateMedicationDtoValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty()
                .WithMessage("{PropertyName} is null or empty.");

            RuleFor(p => p.Quantity)
                .GreaterThan(0)
                .WithMessage("{PropertyName} less than or equal 0.");
        }
    }
}
