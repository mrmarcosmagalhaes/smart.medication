﻿using AutoMapper;
using smart.medication.Domain;
using smart.medication.Application.Models.Dto;

namespace smart.medication.Application.Mapping
{
    public class MappingConfig
    {
        public static MapperConfiguration RegisterMaps()
        {
            var mappingConfig = new MapperConfiguration(config =>
            {
                config.CreateMap<MedicationDto, Medication>();
                config.CreateMap<Medication, MedicationDto>();
            });

            return mappingConfig;
        }
    }
}
