﻿using smart.medication.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace smart.medication.Application.Contracts.Persistence
{
    public interface IMedicationRepository
    {
        Task<IEnumerable<Medication>> GetAll();

        Task<Medication> Add(Medication medication);

        void Delete(Medication medicationId);

        Task<Medication> Get(int medicationId);
    }
}
