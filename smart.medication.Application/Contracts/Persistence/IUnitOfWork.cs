﻿using System;
using System.Threading.Tasks;

namespace smart.medication.Application.Contracts.Persistence
{
    public interface IUnitOfWork : IDisposable
    {
        IMedicationRepository MedicationRepository { get; }

        Task Save();
    }
}
