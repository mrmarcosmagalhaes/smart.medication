﻿using MediatR;
using smart.medication.Application.Models.Dto;
using smart.medication.Application.Responses;
using System.Collections.Generic;

namespace smart.medication.Application.Features.Medication.Requests.Queries
{
    public class GetMedicationListRequest : IRequest<ResponseDto>
    {
        
    }
}
