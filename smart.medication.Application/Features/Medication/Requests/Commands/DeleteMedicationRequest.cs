﻿using MediatR;
using smart.medication.Application.Responses;

namespace smart.medication.Application.Features.Medication.Requests.Queries
{
    public class DeleteMedicationRequest : IRequest<ResponseDto>
    {
        public int MedicationId { get; set; }
    }
}
