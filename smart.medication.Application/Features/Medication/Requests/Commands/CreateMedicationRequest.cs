﻿using MediatR;
using smart.medication.Application.Models.Dto;
using smart.medication.Application.Responses;

namespace smart.medication.Application.Features.Medication.Requests.Queries
{
    public class CreateMedicationRequest : IRequest<ResponseDto>
    {
        public MedicationDto MedicationDto { get; set; }
    }
}
