﻿using AutoMapper;
using FluentValidation;
using MediatR;
using smart.medication.Application.Contracts.Persistence;
using smart.medication.Application.DTOs.Validators;
using smart.medication.Application.Exceptions;
using smart.medication.Application.Features.Medication.Requests.Queries;
using smart.medication.Application.Models.Dto;
using smart.medication.Application.Responses;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ValidationException = smart.medication.Application.Exceptions.ValidationException;

namespace smart.medication.Application.Features.Medication.Handlers.Queries
{
    public class CreateMedicationRequestHandler : IRequestHandler<CreateMedicationRequest, ResponseDto>
    {
        private readonly IUnitOfWork _unitOfWork;

        private IMapper _mapper;

        private IValidator<MedicationDto> _validator;

        public CreateMedicationRequestHandler(IUnitOfWork unitOfWork, IMapper mapper, IValidator<MedicationDto> validator)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _validator = validator;
        }

        public async Task<ResponseDto> Handle(CreateMedicationRequest request, CancellationToken cancellationToken)
        {
            
            var validationResult = await _validator.ValidateAsync(request.MedicationDto);
            if (validationResult.IsValid == false)
            {
                throw new ValidationException(validationResult);
            }
            
            Domain.Medication medication = _mapper.Map<MedicationDto, Domain.Medication>(request.MedicationDto);
            var result = await _unitOfWork.MedicationRepository.Add(medication);
            await _unitOfWork.Save();

            var response = new ResponseDto()
            {
                IsSuccess = true,
                Result = _mapper.Map<Domain.Medication, MedicationDto>(result)
            };
            return response;
        }
    }
}
