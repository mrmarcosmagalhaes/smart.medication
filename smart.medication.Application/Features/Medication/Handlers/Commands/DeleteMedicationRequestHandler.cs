﻿using MediatR;
using smart.medication.Application.Contracts.Persistence;
using smart.medication.Application.Exceptions;
using smart.medication.Application.Features.Medication.Requests.Queries;
using smart.medication.Application.Responses;
using System.Threading;
using System.Threading.Tasks;

namespace smart.medication.Application.Features.Medication.Handlers.Queries
{
    public class DeleteMedicationRequestHandler : IRequestHandler<DeleteMedicationRequest, ResponseDto>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeleteMedicationRequestHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ResponseDto> Handle(DeleteMedicationRequest request, CancellationToken cancellationToken)
        {
            var medication = await _unitOfWork.MedicationRepository.Get(request.MedicationId);

            if (medication == null)
            {
                throw new NotFoundException(nameof(Medication), request.MedicationId);
            }

            _unitOfWork.MedicationRepository.Delete(medication);
            await _unitOfWork.Save();

            ResponseDto responseDto = new ResponseDto()
            {
                IsSuccess = true
            };
            return responseDto;
        }
    }
}
