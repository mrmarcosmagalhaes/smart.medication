﻿using AutoMapper;
using MediatR;
using smart.medication.Application.Contracts.Persistence;
using smart.medication.Application.Features.Medication.Requests.Queries;
using smart.medication.Application.Models.Dto;
using smart.medication.Application.Responses;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace smart.medication.Application.Features.Medication.Handlers.Queries
{
    public class GetMedicationListRequestHandler : IRequestHandler<GetMedicationListRequest, ResponseDto>
    {
        private readonly IUnitOfWork _unitOfWork;

        private IMapper _mapper;

        public GetMedicationListRequestHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ResponseDto> Handle(GetMedicationListRequest request, CancellationToken cancellationToken)
        {
            IEnumerable<smart.medication.Domain.Medication> medications = await _unitOfWork.MedicationRepository.GetAll();
            ResponseDto responseDto = new ResponseDto()
            {
                IsSuccess = true,
                Result = _mapper.Map<IEnumerable<MedicationDto>>(medications)
            };
            return responseDto;
        }
    }
}
