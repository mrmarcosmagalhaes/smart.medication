﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using smart.medication.Application.Features.Medication.Requests.Queries;
using smart.medication.Application.Models.Dto;
using smart.medication.Application.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace smart.medication.api.Controllers
{
    [ApiController]
    [Route("api/medications")]
    public class MedicationController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MedicationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ResponseDto> Get()
        {
            return await _mediator.Send(new GetMedicationListRequest() { });
        }

        [HttpPost]
        public async Task<ResponseDto> Post([FromBody] MedicationDto medicationDto)
        {
            return await _mediator.Send(new CreateMedicationRequest() { MedicationDto = medicationDto });
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<ResponseDto> Delete(int id)
        {
            return await _mediator.Send(new DeleteMedicationRequest() { MedicationId = id });
        }
    }
}
