﻿using Moq;
using smart.medication.Application.Models.Dto;
using smart.medication.Application.Contracts.Persistence;
using smart.medication.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace smart.medication.api.tests.Mock.Repository
{
    public static class MockMedicationRepository
    {
        public static Mock<IMedicationRepository> GetMedicationRepository()
        {
            var medications = new List<Medication>
            {
                new Medication {
                    MedicationId = 1,
                    Name = "Benuron",
                    Quantity = 10,
                    CreationDate = System.DateTime.Now
                },
                new Medication { 
                    MedicationId = 2, 
                    Name = "Brufen", 
                    Quantity = 20, 
                    CreationDate = System.DateTime.Now 
                }
            };

            var mockRepo = new Mock<IMedicationRepository>();

            mockRepo.Setup(r => r.Get(It.IsAny<int>())).ReturnsAsync((int medicationId) =>
            {
                return medications.FirstOrDefault<Medication>(x => x.MedicationId == medicationId);

            });
            mockRepo.Setup(r => r.GetAll()).ReturnsAsync(medications);

            mockRepo.Setup(r => r.Add(It.IsAny<Medication>())).ReturnsAsync((Medication medication) =>
            {
                medications.Add(medication);
                return medication;
            });

            mockRepo.Setup(r => r.Delete(It.IsAny<Medication>())).Callback((Medication medication) =>
            {
                medications.Remove(medication);
            });

            return mockRepo;
        }
    }
}
