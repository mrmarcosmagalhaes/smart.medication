﻿using Moq;
using smart.medication.api.tests.Mock.Repository;
using smart.medication.Application.Contracts.Persistence;

namespace smart.medication.api.tests.Mocks
{
    public static class MockUnitOfWork
    {
        public static Mock<IUnitOfWork> GetUnitOfWork()
        {
            var mockUow = new Mock<IUnitOfWork>();
            var mockMedicationRepo = MockMedicationRepository.GetMedicationRepository();

            mockUow.Setup(r => r.MedicationRepository).Returns(mockMedicationRepo.Object);

            return mockUow;
        }
    }
}
