﻿using AutoMapper;
using Moq;
using Shouldly;
using smart.medication.api.tests.Mocks;
using smart.medication.Application.Contracts.Persistence;
using smart.medication.Application.Exceptions;
using smart.medication.Application.Features.Medication.Handlers.Queries;
using smart.medication.Application.Features.Medication.Requests.Queries;
using smart.medication.Application.Mapping;
using smart.medication.Application.Models.Dto;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace smart.medication.api.tests.Medications.Queries
{
    public class DeleteMedicationRequestHandlerTests
    {
        private readonly IMapper _mapper;

        private readonly Mock<IUnitOfWork> _mockUow;

        public DeleteMedicationRequestHandlerTests()
        {
            _mockUow = MockUnitOfWork.GetUnitOfWork();

            var mapperConfig = MappingConfig.RegisterMaps();
            _mapper = mapperConfig.CreateMapper();
        }

        [Fact]
        public async Task Valid_Medication_Deleted()
        {
            // arrange
            var medicationId = 1;
            var handlerDelete = new DeleteMedicationRequestHandler(_mockUow.Object);

            // act
            await handlerDelete.Handle(new DeleteMedicationRequest() { MedicationId = medicationId }, CancellationToken.None);

            // assert
            IEnumerable<Domain.Medication> medications = await _mockUow.Object.MedicationRepository.GetAll();
            medications.Count<Domain.Medication>().ShouldBe(1);
        }

        [Fact]
        public async Task InValid_Medication_Deleted()
        {
            // arrange
            var medicationId = 3;
            var handlerDelete = new DeleteMedicationRequestHandler(_mockUow.Object);

            // act & assert 
            Should.Throw<NotFoundException>(async () =>
            {
                await handlerDelete.Handle(new DeleteMedicationRequest() { MedicationId = medicationId }, CancellationToken.None);
            });

            // assert
            IEnumerable<Domain.Medication> medications = await _mockUow.Object.MedicationRepository.GetAll();
            medications.Count<Domain.Medication>().ShouldBe(2);
        }
    }
}
