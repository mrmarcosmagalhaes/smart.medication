﻿using AutoMapper;
using Moq;
using Shouldly;
using smart.medication.api.tests.Mocks;
using smart.medication.Application.Contracts.Persistence;
using smart.medication.Application.DTOs.Validators;
using smart.medication.Application.Features.Medication.Handlers.Queries;
using smart.medication.Application.Features.Medication.Requests.Queries;
using smart.medication.Application.Mapping;
using smart.medication.Application.Models.Dto;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace smart.medication.tests.Medications.Queries
{
    public class CreateMedicationRequestHandlerTests
    {
        private readonly IMapper _mapper;

        private readonly Mock<IUnitOfWork> _mockUow;

        private MedicationDto _medicationDto;

        public CreateMedicationRequestHandlerTests()
        {
            _mockUow = MockUnitOfWork.GetUnitOfWork();

            var mapperConfig = MappingConfig.RegisterMaps();
            _mapper = mapperConfig.CreateMapper();
            
            _medicationDto = new MedicationDto
            {
                Name = "Ibuprofeno",
                Quantity = 100
            };
        }

        [Fact]
        public async Task Valid_Medication_Added()
        {
            // arrange
            var validator = new CreateMedicationDtoValidator();
            var handlerAdd = new CreateMedicationRequestHandler(_mockUow.Object, _mapper, validator);
            
            // act
            var resultAdd = await handlerAdd.Handle(new CreateMedicationRequest() { MedicationDto = _medicationDto }, CancellationToken.None);
            
            // assert
            resultAdd.IsSuccess.ShouldBeTrue();
            resultAdd.ShouldNotBeNull();
        }

        [Fact]
        public async Task InValid_Medication_Added()
        {
            // arrange
            _medicationDto.Quantity = 0;
            var validator = new CreateMedicationDtoValidator();
            var handlerAdd = new CreateMedicationRequestHandler(_mockUow.Object, _mapper, validator);

            // act
            var resultAdd = await handlerAdd.Handle(new CreateMedicationRequest() { MedicationDto = _medicationDto }, CancellationToken.None);

            // assert
            resultAdd.IsSuccess.ShouldBeFalse();
            IEnumerable<Domain.Medication> medications = await _mockUow.Object.MedicationRepository.GetAll();
            medications.Count<Domain.Medication>().ShouldBe(2);
        }
    }
}
