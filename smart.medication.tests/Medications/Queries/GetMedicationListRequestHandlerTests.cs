﻿using AutoMapper;
using Moq;
using Shouldly;
using smart.medication.api.tests.Mocks;
using smart.medication.Application.Contracts.Persistence;
using smart.medication.Application.Features.Medication.Handlers.Queries;
using smart.medication.Application.Features.Medication.Requests.Queries;
using smart.medication.Application.Mapping;
using smart.medication.Application.Models.Dto;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace smart.medication.api.tests.Medications.Queries
{
    public class GetMedicationListRequestHandlerTests
    {
        private readonly IMapper _mapper;

        private readonly Mock<IUnitOfWork> _mockUow;

        public GetMedicationListRequestHandlerTests()
        {
            _mockUow = MockUnitOfWork.GetUnitOfWork();

            var mapperConfig = MappingConfig.RegisterMaps();
            _mapper = mapperConfig.CreateMapper();
        }

        [Fact]
        public async Task Valid_Medication_GetAll()
        {
            // arrange
            var handler = new GetMedicationListRequestHandler(_mockUow.Object, _mapper);

            // act
            var result = await handler.Handle(new GetMedicationListRequest(), CancellationToken.None);
            
            // assert
            result.ShouldBeOfType<List<MedicationDto>>();
            result.Count<MedicationDto>().ShouldBe(2);
        }
    }
}
